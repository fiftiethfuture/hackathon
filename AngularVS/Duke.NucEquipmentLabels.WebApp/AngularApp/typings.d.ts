/// <reference types="node" />

declare var require: NodeRequire;
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface JQuery {
    footable(options?: any): any;
    datepicker(options?: any): any;
}
