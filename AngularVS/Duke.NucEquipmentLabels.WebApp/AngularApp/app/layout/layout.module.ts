import {SharedLibrariesModule} from '../helpers/shared-libraries.module';

// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// App
import { LayoutComponent } from './layout/layout.component';
import { LayoutNavbarComponent } from './layout-navbar/layout-navbar.component';
import { LayoutSidenavComponent } from './layout-sidenav/layout-sidenav.component';
import { LayoutFooterComponent } from './layout-footer/layout-footer.component';

// Libs
import { SidenavModule } from '../../vendor/libs/sidenav/sidenav.module';

// Services
import { LayoutService } from '../services/layout.service';

@NgModule({
  imports: [
    RouterModule,
    SidenavModule,
    SharedLibrariesModule
  ],
  declarations: [
    LayoutComponent,
    LayoutNavbarComponent,
    LayoutSidenavComponent,
    LayoutFooterComponent
  ],
  providers: [
    LayoutService
  ]
})
export class LayoutModule { }
