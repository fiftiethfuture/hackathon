import { Component, HostBinding, OnInit } from '@angular/core';
import { AppService } from '../../services/app.service';
import { ConfigurationService } from '../../services/configuration.service';

@Component({
    selector: 'app-layout-footer',
    templateUrl: './layout-footer.component.html',
    styles: [':host { display: block; }']
})
export class LayoutFooterComponent implements OnInit {
    @HostBinding('class.layout-footer') private hostClassMain = true;
    public environment: string;
    public version: string;

    constructor(private appService: AppService,
                private configurationService: ConfigurationService) {}

    ngOnInit() {
        this.environment = this.configurationService.config.environment;
        this.version = this.configurationService.config.version;
    }

    currentBg() {
        return `bg-${this.appService.layoutFooterBg}`;
    }
}
