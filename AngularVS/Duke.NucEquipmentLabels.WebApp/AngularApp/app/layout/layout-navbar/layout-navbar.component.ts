import { Component, Input, HostBinding, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { AppService } from '../../services/app.service';
import { LayoutService } from '../../services/layout.service';
import { Site } from '../../models/Site';
import { ClientPersistenceService } from '../../services/client-persistence.service';
import { WebApiClientService } from '../../services/web-api-client.service';
import { ApiResponse } from '../../models/ApiResponse';
import { ErrorHandlerService } from '../../services/error-handler.service';
import { Subject, Observable, Subscription } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/of';
import { NgbModal, NgbModalRef, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ConfirmCancelModalComponent } from '../../modals/confirm-cancel-modal/confirm-cancel-modal.component';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-layout-navbar',
  templateUrl: './layout-navbar.component.html',
  styles: [':host { display: block; }']
})
export class LayoutNavbarComponent implements OnInit, DoCheck, OnDestroy {
  private static readonly SITE_KEY = 'site';
  // singleton
  public static current: LayoutNavbarComponent;
  // Observable string source
  private selectedSiteSource: Subject<string>;
  // Observable string stream
    public readonly selectedSiteStream$: Observable<string>;

    @Input() public sidenavToggle = true;
  @HostBinding('class.layout-navbar') private hostClassMain = true;
  public sites$: Observable<Site[]>;
  public isRTL: boolean;
  public isExpanded: boolean;

  private isSiteSelectionReversionRequired: boolean;
  private previousSelectedSite: string;
  private _selectedSite: string;
  public get selectedSite(): string {
    return this._selectedSite;
  }
  public set selectedSite(newSite: string) {
    if (this._selectedSite === newSite) {
      return;
    }
    this.previousSelectedSite = this._selectedSite;
    this._selectedSite = newSite;
    if ((this._selectedSite !== undefined) && (!this.isSiteSelectionReversionRequired) && !this.isShoppingCartEmpty) {
      const modalRef: NgbModalRef = this.modalService.open(ConfirmCancelModalComponent);
      modalRef.componentInstance.title = 'Warning';
      modalRef.componentInstance.body = 'Switching sites will empty your Shopping Cart.<br/><br/>Continue?';
      modalRef.result.then((result) => {
        if (result === 'Confirm') {
          this.clientPersistenceService.saveObjectToClientStorage('shoppingCart', {});
          this.selectedSiteSource.next(this._selectedSite);
        } else {
          this.isSiteSelectionReversionRequired = true;
        }
      }, (reason) => {
        if (reason === ModalDismissReasons.BACKDROP_CLICK || reason === ModalDismissReasons.ESC) {
          this.isSiteSelectionReversionRequired = true;
        }
      });
    } else {
      this.selectedSiteSource.next(this._selectedSite);
      this.isSiteSelectionReversionRequired = false;
    }
  }

  private get isShoppingCartEmpty(): boolean {
    let shoppingCart = this.clientPersistenceService.getObjectFromClientStorage('shoppingCart');
    shoppingCart = shoppingCart && shoppingCart.length ? shoppingCart : [];
    return shoppingCart.length <= 0;
  }

  private subscription: Subscription;

  constructor(private appService: AppService,
              private layoutService: LayoutService,
              private webApiClientService: WebApiClientService,
              private clientPersistenceService: ClientPersistenceService,
              private errorHandlerService: ErrorHandlerService,
              private modalService: NgbModal,
              private router: Router) {
      this.selectedSiteSource = new Subject<string>();
      this.selectedSiteStream$ = this.selectedSiteSource.asObservable();
      if (LayoutNavbarComponent.current !== undefined) {
        const error: Error = new Error('More than one instance of LayoutNavbarComponent created.');
        throw error;
      } else {
        LayoutNavbarComponent.current = this;
      }
      this.isRTL = appService.isRTL;
  }

  ngOnInit() {
    this.subscription = this.router.events.filter((event: RouterEvent) =>
      event instanceof NavigationEnd).subscribe( () => {
          this.selectedSiteSource.next(this.selectedSite);
      });
    this.isExpanded = false;
    this.isSiteSelectionReversionRequired = true;
    this.selectedSite = this.clientPersistenceService.getFromCookie(LayoutNavbarComponent.SITE_KEY) || undefined;
    this.sites$ = this.webApiClientService.getSites().map(
      (data: ApiResponse<Site>) => {
        data = data || new ApiResponse<Site>();
        return data.collection;
      },
      (error: any) => {
        this.errorHandlerService.handleError(error);
      }
    );
  }

  ngDoCheck() {
    if (this.isSiteSelectionReversionRequired) {
      this.selectedSite = this.previousSelectedSite;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  currentBg(): any {
    return `bg-${this.appService.layoutNavbarBg}`;
  }

  toggleSidenav(): void {
    this.layoutService.toggleCollapsed();
  }

  onSiteChange($event: Site): void {
    if ($event === undefined) {
        this.clientPersistenceService.saveToCookie(LayoutNavbarComponent.SITE_KEY, '');
    } else {
        this.clientPersistenceService.saveToCookie(LayoutNavbarComponent.SITE_KEY, this.selectedSite);
    }
  }
}
