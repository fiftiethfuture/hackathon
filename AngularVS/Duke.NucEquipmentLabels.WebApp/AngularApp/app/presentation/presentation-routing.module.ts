// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// App
import { ShopLabelsComponent } from './shop-labels/shop-labels.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { LabelOrdersComponent } from './label-orders/label-orders.component';
import { PrintLabelsComponent } from './print-labels/print-labels.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'shoplabels', component: ShopLabelsComponent },
        { path: 'shoplabels/shoppingcart', component: ShoppingCartComponent },
        { path: 'labelorders', component: LabelOrdersComponent },
        { path: 'printlabels', component: PrintLabelsComponent }
    ])],
    exports: [RouterModule]
})
export class PresentationRoutingModule { }
