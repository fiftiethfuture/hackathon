import { SharedLibrariesModule } from '../helpers/shared-libraries.module';

// Angular
import { NgModule } from '@angular/core';

// App
import { PresentationRoutingModule } from './presentation-routing.module';
import { ShopLabelsComponent } from './shop-labels/shop-labels.component';

// Libs
import { AgGridModule } from 'ag-grid-angular';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { LabelOrdersComponent } from './label-orders/label-orders.component';
import { PrintLabelsComponent } from './print-labels/print-labels.component';

@NgModule({
    imports: [
        SharedLibrariesModule,
        PresentationRoutingModule,
        AgGridModule.withComponents([])
    ],
    declarations: [
        ShopLabelsComponent,
        ShoppingCartComponent,
        LabelOrdersComponent,
        PrintLabelsComponent
    ]
})
export class PresentationModule { }
