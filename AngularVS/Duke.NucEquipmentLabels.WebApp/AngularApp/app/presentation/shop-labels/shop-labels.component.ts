import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, DoCheck, EventEmitter, Output } from '@angular/core';
import { EquipmentSearchCriteria, PropertySet } from '../../models/EquipmentSearchCriteria';
import { LayoutNavbarComponent } from '../../layout/layout-navbar/layout-navbar.component';
import { Subscription, Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions, RowNode } from 'ag-grid-community';
import { Equipment } from '../../models/Equipment';
import { WebApiClientService } from '../../services/web-api-client.service';
import { ApiResponse } from '../../models/ApiResponse';
import { ErrorHandlerService } from '../../services/error-handler.service';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LabelSelectionModalComponent } from '../../modals/label-selection-modal/label-selection-modal.component';
import { LabelSelection, LABEL_SIZES } from '../../models/LabelSelection';
import { ClientPersistenceService } from '../../services/client-persistence.service';
import { LabelItem } from '../../models/LabelItem';

@Component({
  selector: 'app-shop-labels',
  templateUrl: './shop-labels.component.html',
  styles: ['./shop-labels.component.css']
})

export class ShopLabelsComponent implements OnInit, OnDestroy, DoCheck {
    @Output() public loadingSignal: EventEmitter<boolean>;
    public isAdvancedSelected: boolean;
    public areAllAdvancedEmpty: boolean;
    public query: EquipmentSearchCriteria;
    public isAdvancedBtnDisabled: boolean;
    public readonly gridOptions: GridOptions;
    public rowData$: Observable<Equipment[]>;
    public isGridInitialized: boolean;

    private shoppingCart: LabelItem[];
    @ViewChild('btnAdvancedOpts') private btnAdvancedOptions: ElementRef;
    @ViewChild('grid') private grid: AgGridNg2;
    private subscription: Subscription;

    private _isLoading: boolean;
    public get isLoading(): boolean {
      return this._isLoading;
    }
    private setIsLoading(value: boolean) {
      if (value !== this._isLoading) {
        this._isLoading = value;
        this.loadingSignal.emit(this._isLoading);
      }
    }
    public get shoppingCartQuantity(): number {
      let quantity = 0;
      if (this.shoppingCart && this.shoppingCart.length) {
        this.shoppingCart.forEach( (labelItem: LabelItem) => {
          quantity += labelItem.quantity;
        });
      }
      return quantity;
    }

    @HostListener('window:resize', ['$event'])
    onWindowResize() {
        if (this.isGridInitialized) {
            this.grid.gridOptions.api.sizeColumnsToFit();
        }
    }

    constructor(private webApiClientService: WebApiClientService,
                private errorHandler: ErrorHandlerService,
                private modalService: NgbModal,
                private clientPersistenceService: ClientPersistenceService) {
      // following code must be in constructor for initial site selection to be collected
      this.query = new EquipmentSearchCriteria();
      this.subscription = LayoutNavbarComponent.current.selectedSiteStream$.subscribe(
        selectedSite => {
          this.query.site = selectedSite;
          this.shoppingCart = this.clientPersistenceService.getObjectFromClientStorage('shoppingCart');
          this.shoppingCart = this.shoppingCart && this.shoppingCart.length ? this.shoppingCart : [];
        }
      );
      this.gridOptions = <GridOptions>{
        context: {
            componentParent: this
        },
        suppressHorizontalScroll: true,
        suppressCellSelection: true,
        pagination: true,
        paginationPageSize: 25,
        rowSelection: 'multiple',
        rowMultiSelectWithClick: true,
        defaultColDef: {
          sortable: true,
          resizable: true,
          filter: true
        },
        columnDefs: [
            { headerName: 'Unit', field: 'unit'},
            { headerName: 'System', field: 'system' },
            { headerName: 'Equipment Type', field: 'equipmentType' },
            { headerName: 'Equipment Suffix', field: 'equipmentSuffix' },
            { headerName: 'EDB Tag', field: 'edbTag' },
            { headerName: 'ECode', field: 'eCode' },
            { headerName: 'Description', field: 'description' }
        ]
      };
    }

    ngOnInit() {
      this.loadingSignal = new EventEmitter<boolean>();
        this.isAdvancedSelected = false;
        this.areAllAdvancedEmpty = true;
        if (this.query.site === undefined) {
          this.isAdvancedBtnDisabled = true;
        }
        this.rowData$ = new Observable<Equipment[]>();
        this.shoppingCart = this.clientPersistenceService.getObjectFromClientStorage('shoppingCart');
        this.shoppingCart = this.shoppingCart && this.shoppingCart.length ? this.shoppingCart : [];
    }

    ngDoCheck() {
      if (this.isGridInitialized && this.isLoading) {
        this.grid.api.showLoadingOverlay();
      }
    }

    ngOnDestroy() {
      this.subscription.unsubscribe();
    }

    onToggleAdvanced(): void {
        this.isAdvancedSelected = !this.isAdvancedSelected;
        const fieldsDiv = document.querySelector('.fields');
        //if (fieldsDiv !== undefined) {
        //    fieldsDiv.classList.toggle('remove-right-margin');
        //}
        if (this.isAdvancedSelected) {
          this.getLookups();
        }
    }

    // TODO: Chrome automatically submits form when entered is pressed
    // On the edb tags textbox. Detect when useragent is chrome and disable
    // enter key handler.
    onSubmit(isValid: boolean): void {
        if (!isValid || !this.query.site || this.query.site === '') {
          return;
        }
        document.getElementById('tags').blur();
        this.rowData$ = this.webApiClientService.getEquipment(this.query).map(
          (data: ApiResponse<Equipment>) => {
            data = data || new ApiResponse<Equipment>();
            this.setIsLoading(false);
            return data.collection;
          },
          (error: any) => {
            this.setIsLoading(false);
            this.errorHandler.handleError(error);
          }
        );
        this.setIsLoading(true);
    }

    onClearAll(): void {
        this.rowData$ = new Observable<Equipment[]>();
        const tempSite = this.query.site;
        this.query = new EquipmentSearchCriteria();
        this.query.site = tempSite;
        this.isAdvancedSelected = false;
    }

    inspectForEmptyFields(): void {
        const advancedProps = this.query.toArray();

        for (let i = 0; i < advancedProps.length; i++) {
            // If any of these conditions are true the property is "empty",
            // so invert the condition to determine if any are not-empty
            if (!(this.query[advancedProps[i]] === '' ||
                this.query[advancedProps[i]] === undefined ||
                this.query[advancedProps[i]] === null ||
                this.query[advancedProps[i]].toString() === '')) {
                this.areAllAdvancedEmpty = false;
                return;
            }
        }
        this.areAllAdvancedEmpty = true;
    }

    onClear(): void {
        this.query.clear(PropertySet.Advanced);
    }

    onGridReady(): void {
      this.grid.api.showNoRowsOverlay();
      this.grid.gridOptions.api.sizeColumnsToFit();
      this.isGridInitialized = true;
    }

    onAddToCart(): void {
      let labelSelections: LabelSelection[] = [];
      const modalRef: NgbModalRef = this.modalService.open(LabelSelectionModalComponent);
      modalRef.result.then((result) => {
        labelSelections = result || [];
        if (labelSelections.length > 0) {
          this.shoppingCart = this.clientPersistenceService.getObjectFromClientStorage('shoppingCart');
          this.shoppingCart = this.shoppingCart && this.shoppingCart.length ? this.shoppingCart : [];
          this.grid.api.getSelectedNodes().forEach( (row: RowNode) => {
            for (const labelSelection of labelSelections) {
              for (const property in labelSelection) {
                if (property.includes('Quantity') && +labelSelection[property] > 0) {
                  const labelItem: LabelItem = new LabelItem();
                  labelItem.quantity = +labelSelection[property];
                  labelItem.type = labelSelection.type;
                  labelItem.isInContainment = labelSelection.isInContainment;
                  labelItem.isNonFieldWorkEC = labelSelection.isNonFieldWorkEC;
                  labelItem.isTempLabelInstalled = labelSelection.isTempLabelInstalled;
                  labelItem.isAdheresToStainlessSteel = labelSelection.isAdheresToStainlessSteel;
                  labelItem.isRFRY = labelSelection.isRFRY;
                  labelItem.isRadWaste = labelSelection.isRadWaste;

                  switch (property) {
                    case 'oneByThreeQuantity':
                      labelItem.size = LABEL_SIZES.OneByThree;
                      break;
                    case 'twoByFourQuantity':
                      labelItem.size = LABEL_SIZES.TwoByFour;
                      break;
                    case 'threeByFiveQuantity':
                      labelItem.size = LABEL_SIZES.ThreeByFive;
                      break;
                    case 'fourBySixQuantity':
                      labelItem.size = LABEL_SIZES.FourBySix;
                      break;
                    case 'sixByTwelveQuantity':
                      labelItem.size = LABEL_SIZES.SixByTwelve;
                      break;
                    case 'eightByTwelveQuantity':
                      labelItem.size = LABEL_SIZES.EightByTwelve;
                      break;
                    case 'nineBySixteenQuantity':
                      labelItem.size = LABEL_SIZES.NineBySixteen;
                      break;
                  }

                  labelItem.updatePropertiesWithIEquipment(row.data);
                  this.shoppingCart.push(labelItem);
                }
              }
            }
          });
          this.clientPersistenceService.saveObjectToClientStorage('shoppingCart', this.shoppingCart);
          this.grid.api.deselectAll();
        }
      }, (reason) => {
        if (reason === ModalDismissReasons.BACKDROP_CLICK || reason === ModalDismissReasons.ESC) {
          labelSelections = [];
        }
      });
    }

    private getLookups(clearSet: number = PropertySet.Facility_Dependent): void {
        if (this.query.site === undefined) {
            // highlight site dropdown red
            return;
        }
        this.query.clear(clearSet);
    }
}
