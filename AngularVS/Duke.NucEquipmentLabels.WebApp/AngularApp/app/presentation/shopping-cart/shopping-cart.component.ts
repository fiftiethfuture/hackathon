import { Component, OnInit, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { Subscription } from 'rxjs';
import { LayoutNavbarComponent } from '../../layout/layout-navbar/layout-navbar.component';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions, CellClickedEvent, CellEditingStoppedEvent } from 'ag-grid-community';
import { WebApiClientService } from '../../services/web-api-client.service';
import { ErrorHandlerService } from '../../services/error-handler.service';
import { ClientPersistenceService } from '../../services/client-persistence.service';
import { LabelItem } from '../../models/LabelItem';
import { ADDITIONAL_LABEL_INFO } from '../../models/LabelSelection';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { InformationModalComponent } from '../../modals/information-modal/information-modal.component';
import { ConfirmCancelModalComponent } from '../../modals/confirm-cancel-modal/confirm-cancel-modal.component';

@Component({
    selector: 'app-shopping-cart',
    templateUrl: './shopping-cart.component.html'
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
    @ViewChildren(AgGridNg2) public grids: QueryList<AgGridNg2>;
    public readonly gridOptions: GridOptions;
    public rowDataGroups: LabelItem[][];
    public areGridsInitialized: boolean;
    public shoppingCart: LabelItem[];
    public totalQuantity: number;
    private subscription: Subscription;
    private gridReadyCount: number;

    constructor(private webApiClientService: WebApiClientService,
                private errorHandler: ErrorHandlerService,
                private clientPersistenceService: ClientPersistenceService,
                private modalService: NgbModal) {
        this.subscription = LayoutNavbarComponent.current.selectedSiteStream$.subscribe(
            () => {
                this.refreshShoppingCart();
            }
        );
        const cellRenderer = (params: any) => {
            return `<input type='checkbox' ${params.value ? 'checked' : ''} />`;
        };
        this.gridOptions = <GridOptions>{
            context: {
                componentParent: this
            },
            defaultColDef: {
                sortable: true,
                resizable: true,
                filter: true
            },
            columnDefs: [
                { headerName: 'Quantity', field: 'quantity', editable: true, cellClass: 'editable-cell' },
                { headerName: 'EDB Tag', field: 'edbTag' },
                { headerName: 'Unit', field: 'unit' },
                { headerName: 'System', field: 'system' },
                { headerName: 'Equipment Type', field: 'equipmentType' },
                { headerName: 'Equipment Suffix', field: 'equipmentSuffix' },
                { headerName: 'ECode', field: 'eCode' },
                { headerName: 'Description', field: 'description' },
                { headerName: ADDITIONAL_LABEL_INFO.InContainment, field: 'isInContainment',
                    cellRenderer: cellRenderer,  cellClass: 'editable-cell' },
                { headerName: ADDITIONAL_LABEL_INFO.NonFieldWorkEC, field: 'isNonFieldWorkEC',
                    cellRenderer: cellRenderer,  cellClass: 'editable-cell' },
                { headerName: ADDITIONAL_LABEL_INFO.TempLabelInstalled, field: 'isTempLabelInstalled',
                    cellRenderer: cellRenderer,  cellClass: 'editable-cell' },
                { headerName: ADDITIONAL_LABEL_INFO.AdheresToStainlessSteel, field: 'isAdheresToStainlessSteel',
                    cellRenderer: cellRenderer,  cellClass: 'editable-cell' },
                { headerName: ADDITIONAL_LABEL_INFO.RFRY, field: 'isRFRY',
                    cellRenderer: cellRenderer,  cellClass: 'editable-cell'},
                { headerName: ADDITIONAL_LABEL_INFO.RadWaste, field: 'isRadWaste',
                    cellRenderer: cellRenderer,  cellClass: 'editable-cell' }
            ]
        };
    }
    ngOnInit() {
        this.totalQuantity = 0;
        this.gridReadyCount = 0;
        this.areGridsInitialized = false;
        this.refreshShoppingCart();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    onGridReady(): void {
        this.gridReadyCount++;
        if (this.gridReadyCount === this.grids.length) {
            this.areGridsInitialized = true;
        }
    }

    onCellClicked(event: CellClickedEvent): void {
        const labelItem: LabelItem = event.data;
        let isRefreshNeeded = true;
        switch (event.colDef.headerName) {
            case ADDITIONAL_LABEL_INFO.InContainment:
                labelItem.isInContainment = !labelItem.isInContainment;
                break;
            case ADDITIONAL_LABEL_INFO.NonFieldWorkEC:
                labelItem.isNonFieldWorkEC = !labelItem.isNonFieldWorkEC;
                break;
            case ADDITIONAL_LABEL_INFO.TempLabelInstalled:
                labelItem.isTempLabelInstalled = !labelItem.isTempLabelInstalled;
                break;
            case ADDITIONAL_LABEL_INFO.AdheresToStainlessSteel:
                labelItem.isAdheresToStainlessSteel = !labelItem.isAdheresToStainlessSteel;
                break;
            case ADDITIONAL_LABEL_INFO.RFRY:
                labelItem.isRFRY = !labelItem.isRFRY;
                break;
            case ADDITIONAL_LABEL_INFO.RadWaste:
                labelItem.isRadWaste = !labelItem.isRadWaste;
                break;
            default:
                isRefreshNeeded = false;
        }
        if (isRefreshNeeded) {
            event.api.refreshCells();
        }
    }

    onCellEditingStopped(event: CellEditingStoppedEvent): void {
        const labelItem: LabelItem = event.data;
        let modalRef: NgbModalRef;
        let modalTitle = 'Invalid Input';
        let modalBody: string;
        if (!(/^\d+$/.test(labelItem[event.colDef.field]))) {
            labelItem[event.colDef.field] = 0;
            event.api.refreshCells();
            modalBody = 'Non-numeric, negative, or empty quantity entered.';
        } else if (+labelItem[event.colDef.field] > 99) {
            labelItem[event.colDef.field] = '99';
            event.api.refreshCells();
            modalBody = 'Quantity greater than 99 entered.';
        } else if (+labelItem[event.colDef.field] === 0) {
            modalTitle = 'Remove Label Item'
            modalBody = 'Quantity of 0 entered. Label item will be removed from shopping cart.<br><br>Continue?';
        } else {
            modalTitle = null;
            this.clientPersistenceService.saveObjectToClientStorage('shoppingCart', this.shoppingCart);
            this.refreshShoppingCart();
        }
        if (modalTitle === 'Invalid Input') {
            modalRef = this.modalService.open(InformationModalComponent);
            modalRef.componentInstance.title = modalTitle;
            modalRef.componentInstance.body = modalBody;
        } else if (modalTitle === 'Remove Label Item') {
            modalRef = this.modalService.open(ConfirmCancelModalComponent);
            modalRef.componentInstance.title = modalTitle;
            modalRef.componentInstance.body = modalBody;
            modalRef.result.then((result) => {
                if (result === 'Confirm') {
                    const index = this.shoppingCart.indexOf(labelItem, 0);
                    if (index > -1) {
                        this.shoppingCart.splice(index, 1);
                    }
                    this.clientPersistenceService.saveObjectToClientStorage('shoppingCart', this.shoppingCart);
                    this.refreshShoppingCart();
                } else {
                    labelItem[event.colDef.field] = '1';
                    event.api.refreshCells();
                }
                }, (reason) => {
                if (reason === ModalDismissReasons.BACKDROP_CLICK || reason === ModalDismissReasons.ESC) {
                    labelItem[event.colDef.field] = '1';
                    event.api.refreshCells();
                }
            });
        }
    }

    onEmptyCart() {
        const modalRef: NgbModalRef = this.modalService.open(ConfirmCancelModalComponent);
        modalRef.componentInstance.title = 'Empty Shopping Cart';
        modalRef.componentInstance.body = 'Are you sure you want to empty your shopping cart?';
        modalRef.result.then((result) => {
            if (result === 'Confirm') {
                this.clientPersistenceService.saveObjectToClientStorage('shoppingCart', {});
                this.refreshShoppingCart();
            }
        });
    }

    public buildHeader(rowData: LabelItem[]): string {
        let sectionQuantity = 0;
        let header = '';
        if (rowData) {
            rowData.forEach( (labelItem: LabelItem) => {
                sectionQuantity += +labelItem.quantity;
            });
            header = `${rowData[0].type} ${rowData[0].size} ${rowData[0].color} (${sectionQuantity})`;
        }
        return header;
    }

    private refreshShoppingCart(): void {
        this.shoppingCart = this.clientPersistenceService.getObjectFromClientStorage('shoppingCart');
        this.shoppingCart = this.shoppingCart && this.shoppingCart.length ? this.shoppingCart : [];
        this.totalQuantity = 0;
        this.shoppingCart.forEach( (labelItem: LabelItem) => {
            this.totalQuantity += +labelItem.quantity;
        });
        this.rowDataGroups = this.groupBy(this.shoppingCart, function (item: any) {
            return [item.type, item.size, item.color];
        });
        this.sortRowDataGroups();
    }

    private groupBy(array: any[], func: any) {
        const groups = {};
        array.forEach(function (o) {
            const group = JSON.stringify(func(o));
            groups[group] = groups[group] || [];
            groups[group].push(o);
        });
        return Object.keys(groups).map(function (group) {
            return groups[group];
        });
    }

    private sortRowDataGroups(): void {
        // tertiary sort
        this.rowDataGroups.sort((a: LabelItem[], b: LabelItem[]) => {
            return a[0].color > b[0].color ? 1 : -1;
        });
        // secondary sort
        this.rowDataGroups.sort((a: LabelItem[], b: LabelItem[]) => {
            return a[0].size < b[0].size ? 1 : -1;
        });
        // primary sort
        this.rowDataGroups.sort((a: LabelItem[], b: LabelItem[]) => {
            return a[0].type > b[0].type ? 1 : -1;
        });
    }
}
