
export interface ILabelInfo {
    type: string;
    isInContainment: boolean;
    isNonFieldWorkEC: boolean;
    isTempLabelInstalled: boolean;
    isAdheresToStainlessSteel: boolean;
    isRFRY: boolean;
    isRadWaste: boolean;
}