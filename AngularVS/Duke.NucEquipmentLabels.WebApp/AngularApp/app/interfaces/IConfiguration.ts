
export interface IConfiguration {
    nucEquipmentLabelsApiUrl: string;
    helpUrl: string;
    environment: string;
    version: string;
}
