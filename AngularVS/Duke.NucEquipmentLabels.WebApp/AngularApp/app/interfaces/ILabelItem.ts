
export interface ILabelItem {
    labelItemId: string;
    status: string;
    quantity: number;
    size: string;
    type: string;
    color: string;
}
