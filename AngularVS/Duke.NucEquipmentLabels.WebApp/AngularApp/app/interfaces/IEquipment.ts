
export interface IEquipment {
    site: string;
    edbTag: string;
    unit: string;
    system: string;
    equipmentType: string;
    equipmentSuffix: string;
    componentType: string;
    componentSuffix: string;
    eCode: string;
    description: string;
    operationalControlGroup: string;
    penetrationNumber: string;
    primaryPowerSupply: string;
}
