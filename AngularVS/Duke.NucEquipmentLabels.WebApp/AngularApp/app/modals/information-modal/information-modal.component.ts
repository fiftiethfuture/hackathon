import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-information-modal',
    templateUrl: './information-modal.component.html'
})
export class InformationModalComponent {
    public title: string;
    public body: string;
    public set dismissAfterMilliseconds(value: number) {
        this.timer = setTimeout(() => {this.activeModal.dismiss();}, value);
    }

    private timer: NodeJS.Timer;

    constructor(public activeModal: NgbActiveModal) {}
}
