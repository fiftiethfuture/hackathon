import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirm-cancel-modal',
  templateUrl: './confirm-cancel-modal.component.html'
})
export class ConfirmCancelModalComponent implements OnInit {
  public title: string;
  public body: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
}
