import { Component, OnInit, ViewChild, DoCheck } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { LABEL_SIZES, LABEL_TYPES, LabelSelection, ADDITIONAL_LABEL_INFO } from '../../models/LabelSelection';

@Component({
  selector: 'app-label-selection-modal',
  templateUrl: './label-selection-modal.component.html',
  styles: ['./label-selection-modal.component.scss']
})
export class LabelSelectionModalComponent implements OnInit {
  public isGridInitialized: boolean;
  public readonly gridOptions: GridOptions;
  public wasNonNumericalValueEntered: boolean;
  public wasGreaterThan99QuantityEntered: boolean;
  public checkBoxIds = ADDITIONAL_LABEL_INFO;

  @ViewChild('grid') private grid: AgGridNg2;

  constructor(public activeModal: NgbActiveModal) {
    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      },
      suppressHorizontalScroll: true,
      suppressRowClickSelection: false,
      suppressRowHoverHighlight: true,
      suppressLoadingOverlay: true,
      suppressNoRowsOverlay: true,
      stopEditingWhenGridLosesFocus: true,
      defaultColDef: {
        sortable: false,
        resizable: false,
        filter: false,
        suppressMovable: true,
        editable: true
      },
      columnDefs: [
          { headerName: '', field: 'type', editable: false},
          { headerName: `${LABEL_SIZES.OneByThree}`, field: 'oneByThreeQuantity', cellClass: 'editable-cell' },
          { headerName: `${LABEL_SIZES.TwoByFour}`, field: 'twoByFourQuantity', cellClass: 'editable-cell' },
          { headerName: `${LABEL_SIZES.ThreeByFive}`, field: 'threeByFiveQuantity', cellClass: 'editable-cell' },
          { headerName: `${LABEL_SIZES.FourBySix}`, field: 'fourBySixQuantity', cellClass: 'editable-cell' },
          { headerName: `${LABEL_SIZES.SixByTwelve}`, field: 'sixByTwelveQuantity', cellClass: 'editable-cell' },
          { headerName: `${LABEL_SIZES.EightByTwelve}`, field: 'eightByTwelveQuantity', cellClass: 'editable-cell' },
          { headerName: `${LABEL_SIZES.NineBySixteen}`, field: 'nineBySixteenQuantity', cellClass: 'editable-cell' }
      ],
      rowData: [
        new LabelSelection(LABEL_TYPES.Adhesive),
        new LabelSelection(LABEL_TYPES.Procelain),
        new LabelSelection(LABEL_TYPES.Tiger)
      ]
    };
  }

  ngOnInit() {
    this.wasNonNumericalValueEntered = false;
    this.wasGreaterThan99QuantityEntered = false;
  }

  onCellEditingStopped(event: any): void {
    const labelSelection: LabelSelection = event.data;
    if (!(/^\d+$/.test(labelSelection[event.colDef.field]))) {
      labelSelection[event.colDef.field] = 0;
      this.grid.api.refreshCells();
      this.wasNonNumericalValueEntered = true;
    } else if (+labelSelection[event.colDef.field] > 99) {
      labelSelection[event.colDef.field] = '99';
      this.grid.api.refreshCells();
      this.wasGreaterThan99QuantityEntered = true;
    }
  }

  onCheckBoxClicked(event: any): void {
    this.grid.gridOptions.rowData.forEach(row => {
      let labelSelection = new LabelSelection();
      row = typeof(row) === typeof(labelSelection) ? row : labelSelection;
      labelSelection = row;
      switch (event.currentTarget.id) {
        case ADDITIONAL_LABEL_INFO.InContainment:
          labelSelection.isInContainment = event.currentTarget.checked;
          break;
        case ADDITIONAL_LABEL_INFO.NonFieldWorkEC:
          labelSelection.isNonFieldWorkEC = event.currentTarget.checked;
          break;
        case ADDITIONAL_LABEL_INFO.TempLabelInstalled:
          labelSelection.isTempLabelInstalled = event.currentTarget.checked;
          break;
        case ADDITIONAL_LABEL_INFO.AdheresToStainlessSteel:
          labelSelection.isAdheresToStainlessSteel = event.currentTarget.checked;
          break;
        case ADDITIONAL_LABEL_INFO.RFRY:
          labelSelection.isRFRY = event.currentTarget.checked;
          break;
        case ADDITIONAL_LABEL_INFO.RadWaste:
          labelSelection.isRadWaste = event.currentTarget.checked;
          break;
      }
    });
  }

  onGridReady(): void {
    this.grid.gridOptions.api.sizeColumnsToFit();
    this.isGridInitialized = true;
  }
}
