// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// App
import { LayoutComponent } from './layout/layout/layout.component';

// Routes
const routes: Routes = [
  // Default
  { path: '', redirectTo: '/shoplabels', pathMatch: 'full' },

  // Presentation
  { path: '', component: LayoutComponent, loadChildren: './presentation/presentation.module#PresentationModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
