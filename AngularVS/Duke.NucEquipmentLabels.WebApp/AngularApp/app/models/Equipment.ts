import { ParameterizableModel } from './ParameterizableModel';
import { IEquipment } from '../interfaces/IEquipment';

export class Equipment extends ParameterizableModel implements IEquipment {
    public site: string;
    public edbTag: string;
    public unit: string;
    public system: string;
    public equipmentType: string;
    public equipmentSuffix: string;
    public componentType: string;
    public componentSuffix: string;
    public eCode: string;
    public description: string;
    public operationalControlGroup: string;
    public penetrationNumber: string;
    public primaryPowerSupply: string;

    constructor(
        site?: string,
        edbTag?: string,
        unit?: string,
        system?: string,
        equipmentType?: string,
        equipmentSuffix?: string,
        componentType?: string,
        componentSuffix?: string,
        eCode?: string,
        description?: string,
        operationalControlGroup?: string,
        penetrationNumber?: string,
        primaryPowerSupply?: string
    ) {
        super();
        this.site = site || '';
        this.edbTag = edbTag || '';
        this.unit = unit || '';
        this.system = system || '';
        this.equipmentType = equipmentType || '';
        this.equipmentSuffix = equipmentSuffix || '';
        this.componentType = componentType || '';
        this.componentSuffix = componentSuffix || '';
        this.eCode = eCode || '';
        this.description = description || '';
        this.operationalControlGroup = operationalControlGroup || '';
        this.penetrationNumber = penetrationNumber || '';
        this.primaryPowerSupply = primaryPowerSupply || '';
    }
}
