import { ParameterizableModel } from './ParameterizableModel';
import { Key } from 'ts-keycode-enum';

export class PropertySet {
    public static Simple = 0;
    public static Advanced = 1;
    public static Facility_Dependent = 2;
    public static FacilityUnitAndSystem_Dependent = 3;
    public static All = 4;
}

export const SIMPLE_FIELDS: string[] = [
    'facility',
    'EDBTag'
];

export const FACILITY_DEPENDENT_FIELDS: string[] = [
    'system',
    'eqRelated',
    'equipmentType',
    'unit',
    'eqStatus'
];

export const FACILITYSYSTEMUNIT_DEPENDENT_FIELDS: string[] = [
    'eqRelated',
    'equipmentType',
    'componentType'
];

export class EquipmentSearchCriteria extends ParameterizableModel {
    public site: string;
    public edbTag?: string;
    public unit?: string;
    public system?: string;
    public equipmentType?: string;
    public equipmentSuffix?: string;
    public componentType?: string;
    public componentSuffix?: string;

    constructor() {
        super();
    }

    public toArray(set: number = PropertySet.Advanced): string[] {
        let properties = this.getProperties();

        // remove simple search properties
        switch (set) {
            case PropertySet.All:
                return properties;
            case PropertySet.Advanced:
                properties = properties.filter(item => {
                    return SIMPLE_FIELDS.indexOf(item) === -1;
                });
                return properties;
            default:
                console.error('Invalid argument value for method toArray() method in SearchQuery.');
                return [];
        }
    }

    public clear(set: number): void {
        let properties = this.getProperties();
        switch (set) {
            case PropertySet.Simple:
                properties = properties.filter(item => {
                    return SIMPLE_FIELDS.indexOf(item) !== -1;
                });
                break;
            case PropertySet.Advanced:
                properties = properties.filter(item => {
                    return SIMPLE_FIELDS.indexOf(item) === -1;
                });
                break;
            case PropertySet.Facility_Dependent:
                properties = properties.filter(item => {
                    return FACILITY_DEPENDENT_FIELDS.indexOf(item) !== -1;
                });
                break;
            case PropertySet.FacilityUnitAndSystem_Dependent:
                properties = properties.filter(item => {
                    return FACILITYSYSTEMUNIT_DEPENDENT_FIELDS.indexOf(item) !== -1;
                });
                break;
        }

        for (let i = 0; i < properties.length; i++) {
            this[properties[i]] = undefined;
        }
    }

    private getProperties(): string[] {
        const properties = [];
        for (const property in this) {
            if (this.hasOwnProperty(property) && property.charCodeAt(0) > Key.Nine) {
                properties.push(property.toString());
            }
        }

        return properties;
    }
}
