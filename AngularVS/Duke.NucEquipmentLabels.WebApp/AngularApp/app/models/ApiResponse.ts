﻿
export class ApiResponse<T> {
    public collection: T[];
    public count: number;
    public filter: string;
    get isEmpty(): boolean {
        return this.collection.length == 0 && this.count == 0;
    }

    constructor(
        collection?: T[],
        count?: number,
        filter?: string,
    ) {
        this.collection = collection || [];
        this.count = count || 0;
        this.filter = filter || '';
    }
}