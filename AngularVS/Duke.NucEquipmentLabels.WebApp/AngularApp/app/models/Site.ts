﻿
export class Site {
    public siteCode: string;
    public siteDescription: string;

    constructor(siteCode?: string, siteDescription?: string) {
        this.siteCode = siteCode || '';
        this.siteDescription = siteDescription ||  '';
    }
}
