﻿import { IConfiguration } from '../interfaces/IConfiguration';

export class Configuration implements IConfiguration {
    public nucEquipmentLabelsApiUrl: string;
    public helpUrl: string;
    public environment: string;
    public version: string;

    constructor() {
        this.nucEquipmentLabelsApiUrl = '';
        this.helpUrl = '';
        this.environment = '';
        this.version = '';
    }

    public updatePropertiesWithIConfiguration (configuration: IConfiguration): void {
        this.nucEquipmentLabelsApiUrl = configuration && configuration.nucEquipmentLabelsApiUrl &&
            typeof(configuration.nucEquipmentLabelsApiUrl) === 'string' ? configuration.nucEquipmentLabelsApiUrl : '';
        this.helpUrl = configuration && configuration.helpUrl && typeof(configuration.helpUrl) === 'string' ? configuration.helpUrl : '';
        this.environment = configuration && configuration.environment &&
            typeof(configuration.environment) === 'string' ? configuration.environment : '';
    }
}