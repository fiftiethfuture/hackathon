﻿
export class Constants {

    /**
     * Endpoint Constants
     */

    public static readonly ENDPOINT_SITES = '/Sites';
    public static readonly ENDPOINT_EQUIPMENT = '/Equipment';

    /**
     * Misc Constants
     */

    public static readonly ERROR_CODES = {
        BAD_REQUEST: 400,
        NOT_FOUND: 404,
        NOT_ACCEPTABLE: 406,
        NO_CONTENT: 204,
        CONNECTION: 0,
        SERVER_ERROR: 500
    };
}
