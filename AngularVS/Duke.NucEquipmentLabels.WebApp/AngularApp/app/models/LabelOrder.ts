import { ParameterizableModel } from './ParameterizableModel';
import { LabelItem } from './LabelItem';

export const enum ORDER_STATUSES {
    Pending = 'Pending',
    InProgress = 'In Progress',
    Fulfilled = 'Fulfilled'
}

export class LabelOrder extends ParameterizableModel {
    public labelOrderId: string;
    public status: string;
    public labelItems: LabelItem[];

    constructor(
        labelOrderId?: string,
        status?: string,
        labelItems?: LabelItem[]
    ) {
        super();
        this.labelOrderId = labelOrderId || '';
        this.status = status || '';
        this.labelItems = labelItems || [];
    }
}