﻿import { HttpParams } from '@angular/common/http';
import { CustomEncoder } from '../helpers/custom-encoder';

export class ParameterizableModel {
    public toHttpParams(): HttpParams {
        let params = new HttpParams({ encoder: new CustomEncoder() });
        for (const property in this) {
            if (this.hasOwnProperty(property) &&
                this[property] !== undefined &&
                this[property] !== null &&
                this[property].toString() !== '') {
                    params = params.append(property.toString(), this[property].toString());
                }
        }
        return params;
    }
}
