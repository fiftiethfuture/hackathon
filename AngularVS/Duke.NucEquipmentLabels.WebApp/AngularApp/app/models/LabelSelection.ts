import { ILabelInfo } from '../interfaces/ILabelInfo';

export const enum LABEL_SIZES {
    OneByThree = '1x3',
    TwoByFour = '2x4',
    ThreeByFive = '3x5',
    FourBySix = '4x6',
    SixByTwelve = '6x12',
    EightByTwelve = '8x12',
    NineBySixteen = '9x16'
}

export const enum LABEL_TYPES {
    Adhesive = 'ADH',
    Procelain = 'POR',
    Tiger = 'TIG'
}

export enum ADDITIONAL_LABEL_INFO {
    InContainment = 'In Containment',
    NonFieldWorkEC = 'Non-Field Work EC',
    TempLabelInstalled = 'Temp Label Installed',
    AdheresToStainlessSteel = 'Adheres to Stainless Steel',
    RFRY= 'RF/RY (Fire Protection)',
    RadWaste = 'Rad Waste'
}

export class LabelSelection implements ILabelInfo {
    public type: string;
    public oneByThreeQuantity: string;
    public twoByFourQuantity: string;
    public threeByFiveQuantity: string;
    public fourBySixQuantity: string;
    public sixByTwelveQuantity: string;
    public eightByTwelveQuantity: string;
    public nineBySixteenQuantity: string;
    public isInContainment: boolean;
    public isNonFieldWorkEC: boolean;
    public isTempLabelInstalled: boolean;
    public isAdheresToStainlessSteel: boolean;
    public isRFRY: boolean;
    public isRadWaste: boolean;

    constructor(
        type?: string,
        oneAndAHalfByThreeQuantity?: string,
        twoByFourQuantity?: string,
        threeByFiveQuantity?: string,
        fourBySixQuantity?: string,
        sixByTwelveQuantity?: string,
        eightByTwelveQuantity?: string,
        nineBySixteenQuantity?: string,
        isInContainment?: boolean,
        isNonFieldWorkEC?: boolean,
        isTempLabelInstalled?: boolean,
        isAdheresToStainlessSteel?: boolean,
        isRFRY?: boolean,
        isRadWaste?: boolean,
    ) {
        this.type = type || LABEL_TYPES.Adhesive;
        this.oneByThreeQuantity = oneAndAHalfByThreeQuantity || '0';
        this.twoByFourQuantity = twoByFourQuantity || '0';
        this.threeByFiveQuantity = threeByFiveQuantity || '0';
        this.fourBySixQuantity = fourBySixQuantity || '0';
        this.sixByTwelveQuantity = sixByTwelveQuantity || '0';
        this.eightByTwelveQuantity = eightByTwelveQuantity || '0';
        this.nineBySixteenQuantity = nineBySixteenQuantity || '0';
        this.isInContainment = isInContainment || false;
        this.isNonFieldWorkEC = isNonFieldWorkEC || false;
        this.isTempLabelInstalled = isTempLabelInstalled || false;
        this.isAdheresToStainlessSteel = isAdheresToStainlessSteel || false;
        this.isRFRY = isRFRY || false;
        this.isRadWaste = isRadWaste || false;
    }
}
