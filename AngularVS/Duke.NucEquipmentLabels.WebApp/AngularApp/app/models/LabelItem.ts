import { Equipment } from './Equipment';
import { IEquipment } from '../interfaces/IEquipment';
import { ILabelItem } from '../interfaces/ILabelItem';
import { LABEL_TYPES } from './LabelSelection';
import { ILabelInfo } from '../interfaces/ILabelInfo';

export const enum LABEL_STATUSES {
    Pending = 'Pending',
    InProgress = 'In Progress',
    Printed = 'Printed',
    PrintedAltered = 'PrintedAltered'
}

export const enum LABEL_COLORS {
    Black = 'Black',
    Green = 'Green',
    Orange = 'Orange',
    Blue = 'Blue',
    Red = 'Red',
    Yellow = 'Yellow'
}

export class LabelItem extends Equipment implements ILabelItem, ILabelInfo {
    public labelItemId: string;
    public status: string;
    public quantity: number;
    public size: string;
    public type: string;
    public color: string;
    public isInContainment: boolean;
    public isNonFieldWorkEC: boolean;
    public isTempLabelInstalled: boolean;
    public isAdheresToStainlessSteel: boolean;
    public isRFRY: boolean;
    public isRadWaste: boolean;

    constructor(
        labelItemId?: string,
        status?: string,
        quantity?: number,
        size?: string,
        type?: string,
        color?: string,
        isInContainment?: boolean,
        isNonFieldWorkEC?: boolean,
        isTempLabelInstalled?: boolean,
        isAdheresToStainlessSteel?: boolean,
        isRFRY?: boolean,
        isRadWaste?: boolean,
    ) {
        super();
        this.labelItemId = labelItemId || '';
        this.status = status || '';
        this.quantity = quantity || 0;
        this.size = size || '';
        this.type = type || '';
        this.color = color || '';
        this.isInContainment = isInContainment || false;
        this.isNonFieldWorkEC = isNonFieldWorkEC || false;
        this.isTempLabelInstalled = isTempLabelInstalled || false;
        this.isAdheresToStainlessSteel = isAdheresToStainlessSteel || false;
        this.isRFRY = isRFRY || false;
        this.isRadWaste = isRadWaste || false;
    }

    public updatePropertiesWithIEquipment(equipment: IEquipment): void {
        this.site = equipment && equipment.site && typeof(equipment.site) === 'string' ? equipment.site : '';
        this.edbTag = equipment && equipment.edbTag && typeof(equipment.edbTag) === 'string' ? equipment.edbTag : '';
        this.unit = equipment && equipment.unit && typeof(equipment.unit) === 'string' ? equipment.unit : '';
        this.system = equipment && equipment.system && typeof(equipment.system) === 'string' ? equipment.system : '';
        this.equipmentType = equipment && equipment.equipmentType && typeof(equipment.equipmentType) === 'string' ?
            equipment.equipmentType : '';
        this.equipmentSuffix = equipment && equipment.equipmentSuffix && typeof(equipment.equipmentSuffix) === 'string' ?
            equipment.equipmentSuffix : '';
        this.componentType = equipment && equipment.componentType && typeof(equipment.componentType) === 'string' ?
            equipment.componentType : '';
        this.componentSuffix = equipment && equipment.componentSuffix && typeof(equipment.componentSuffix) === 'string' ?
            equipment.componentSuffix : '';
        this.eCode = equipment && equipment.eCode && typeof(equipment.eCode) === 'string' ? equipment.eCode : '';
        this.description = equipment && equipment.description && typeof(equipment.description) === 'string' ? equipment.description : '';
        this.operationalControlGroup = equipment && equipment.operationalControlGroup &&
            typeof(equipment.operationalControlGroup) === 'string' ? equipment.operationalControlGroup : '';
        this.penetrationNumber = equipment && equipment.penetrationNumber && typeof(equipment.penetrationNumber) === 'string' ?
            equipment.penetrationNumber : '';
        this.primaryPowerSupply = equipment && equipment.primaryPowerSupply && typeof(equipment.primaryPowerSupply) === 'string' ?
            equipment.primaryPowerSupply : '';

        if (this.type === LABEL_TYPES.Tiger) {
            this.color = '';
        } else {
            switch (this.unit) {
                case '0':
                    this.color = LABEL_COLORS.Black;
                    break;
                case '1':
                    this.color = LABEL_COLORS.Green;
                    break;
                case '2':
                    this.color = LABEL_COLORS.Orange;
                    break;
                case '3':
                    this.color = LABEL_COLORS.Blue;
                    break;
                default:
                    this.color = LABEL_COLORS.Black;
            }
        }
    }
}
