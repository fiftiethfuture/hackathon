//Angular
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER } from '@angular/core';
import { HttpModule } from '@angular/http';

// Libs
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ContextMenuModule } from 'ngx-contextmenu';
import { TourNgBootstrapModule } from 'ngx-tour-ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { BlockUIModule } from 'ng-block-ui';
import { CalendarModule } from 'angular-calendar';
import { NgxMdModule } from 'ngx-md';
import { AgGridModule } from 'ag-grid-angular';

// App
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { ThemeSettingsModule } from '../vendor/libs/theme-settings/theme-settings.module';
import { AppService } from './services/app.service';
import { AppErrorHandler } from './helpers/app-error.handler';
import { ErrorHandlerService } from './services/error-handler.service';
import { ConfigurationService } from './services/configuration.service';
import { InformationModalComponent } from './modals/information-modal/information-modal.component';
import { ConfirmCancelModalComponent } from './modals/confirm-cancel-modal/confirm-cancel-modal.component';
import { LabelSelectionModalComponent } from './modals/label-selection-modal/label-selection-modal.component';

@NgModule({
  declarations: [
      AppComponent,
      InformationModalComponent,
      ConfirmCancelModalComponent,
      LabelSelectionModalComponent
  ],

  imports: [
    // Angular
    BrowserModule,
    HttpClientModule,
    HttpModule,
    NgbModule.forRoot(),

    // App
    AppRoutingModule,
    LayoutModule,
    ThemeSettingsModule,

    // Libs
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ContextMenuModule.forRoot(),
    TourNgBootstrapModule.forRoot(),
    AgmCoreModule.forRoot({
        /* NOTE: When using Google Maps on your own site you MUST get your own API key:
                https://developers.google.com/maps/documentation/javascript/get-api-key
                After you got the key paste it in the URL below. */
        apiKey: 'AIzaSyCHtdj4L66c05v1UZm-nte1FzUEAN6GKBI'
    }),
    BlockUIModule.forRoot(),
    CalendarModule.forRoot(),
    NgxMdModule.forRoot(),
    AgGridModule.withComponents([])
  ],

  providers: [
      Title,
      AppService,
      ErrorHandlerService,
      { provide: ErrorHandler, useClass: AppErrorHandler },
      // Register our config service as a provided service that
      // components may request.
      ConfigurationService, {
          // Here we request that configuration loading be done at app-
          // initialization time (prior to rendering)
          provide: APP_INITIALIZER, useFactory: (configService: ConfigurationService) => () => configService.loadConfigurationData(),
          deps: [ConfigurationService],
          multi: true
      }
    ],

    entryComponents: [
        InformationModalComponent,
        ConfirmCancelModalComponent,
        LabelSelectionModalComponent
    ],

    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}
