import { ErrorHandler, Inject, Injector, NgZone } from "@angular/core";
import { ConfigurationService } from "../services/configuration.service";
import { AppErrorHandler } from "../helpers/app-error.handler";

export class ErrorHandlerService extends AppErrorHandler implements ErrorHandler {
    constructor(@Inject(NgZone) ngZone: NgZone,
        @Inject(Injector) injector: Injector,
        configurationService: ConfigurationService) {
        super(ngZone, injector, configurationService);
    }
}