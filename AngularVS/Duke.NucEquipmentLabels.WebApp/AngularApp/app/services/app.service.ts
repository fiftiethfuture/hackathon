import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ThemeSettingsService } from '../../vendor/libs/theme-settings/theme-settings.service';

@Injectable()
export class AppService {
  constructor(private titleService: Title, private themeSettingsService: ThemeSettingsService) {}

  // Set page title
  public set pageTitle(value: string) {
    this.titleService.setTitle(`${value}`);
  }
  //Check if platform is mobile
  public get isMobile(): boolean {
      const nonMobilePlatforms = [
          'Win16',
          'Win32',
          'Macintosh',
          'MacIntel'
      ];
      return nonMobilePlatforms.indexOf(navigator.platform) === -1;
  }
  // Check for RTL layout
  public get isRTL(): boolean {
    return document.documentElement.getAttribute('dir') === 'rtl' ||
           document.body.getAttribute('dir') === 'rtl';
  }
  // Check if IE
  //Source: https://stackoverflow.com/questions/17907445/how-to-detect-ie11
  public get isIE(): boolean {
    return ((navigator.appName == 'Microsoft Internet Explorer') || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null)));
  }

  // Layout navbar color
  public get layoutNavbarBg(): string {
    return this.themeSettingsService.getOption('navbarBg') || 'navbar-theme';
  }

  // Layout sidenav color
  public get layoutSidenavBg(): string {
    return this.themeSettingsService.getOption('sidenavBg') || 'sidenav-theme';
  }

  // Layout footer color
  public get layoutFooterBg(): string {
    return this.themeSettingsService.getOption('footerBg') || 'footer-theme';
  }

  // Animate scrollTop
  public scrollTop(to: number, duration: number, element = document.scrollingElement || document.documentElement): void {
    if (element.scrollTop === to) { return; }
    const start = element.scrollTop;
    const change = to - start;
    const startDate = +new Date();

    // t = current time; b = start value; c = change in value; d = duration
    const easeInOutQuad = (t, b, c, d) => {
      t /= d / 2;
      if (t < 1) { return c / 2 * t * t + b; }
      t--;
      return -c / 2 * (t * (t - 2) - 1) + b;
    };

    const animateScroll = function() {
      const currentDate = +new Date();
      const currentTime = currentDate - startDate;
      element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration), 10);
      if (currentTime < duration) {
        requestAnimationFrame(animateScroll);
      } else {
        element.scrollTop = to;
      }
    };

    animateScroll();
  }
}
