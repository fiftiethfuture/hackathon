import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/ApiResponse';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ClientPersistenceService {

    constructor(private configuration: ConfigurationService) { }

    public getFromClientStorage(key: string, strongPersistence?: boolean): string {
        if (this.configuration.debugMode) {
            console.log(`Key received: ${key}`);
        }
        strongPersistence = strongPersistence || false;
        switch (strongPersistence) {
            case false:
                if (this.configuration.debugMode) {
                    console.log(`Retrieved: ${sessionStorage.getItem(key)} from storage.`);
                }
                return sessionStorage.getItem(key);
            case true:
                if (this.configuration.debugMode) {

                } console.log(`Retrieved: ${localStorage.getItem(key)} from storage.`);
                return localStorage.getItem(key);
        }
    }

    public saveToClientStorage(key: string, value: string, strongPersistence?: boolean): void {
        strongPersistence = strongPersistence || false;
        switch (strongPersistence) {
            case false:
                sessionStorage.setItem(key, value);
                if (this.configuration.debugMode) {
                    console.log(`Saved: ${value} to sessionStorage with key: ${key}`);
                }
                break;
            case true:
                localStorage.setItem(key, value);
                if (this.configuration.debugMode) {
                    console.log(`Saved: ${value} to localStorage with key: ${key}`);
                }
                break;
        }
    }

    public saveObjectToClientStorage<T>(key: string, value: T, isStrongPersistence: boolean = true): void {
        // check for null value
        if (value === null || value === undefined) {
            if (this.configuration.debugMode) {
                console.log('Null or undefined value received at saveObjectToClientStorage().');
            }
            return;
        }

        // stringify object, save
        switch (isStrongPersistence) {
            case true:
                localStorage.setItem(key, JSON.stringify(value));
                if (this.configuration.debugMode) {
                    console.log(`Saved: ${value} to localStorage with key: ${key}`);
                }
                break;
            case false:
                sessionStorage.setItem(key, JSON.stringify(value));
                if (this.configuration.debugMode) {
                    console.log(`Saved: ${value} to sessionStorage with key: ${key}`);
                }
                break;
        }
    }

    public getObjectFromClientStorage<T>(key: string, strongPersistence: boolean = true): any {
        let value;
        // get string
        switch (strongPersistence) {
            case true:
                value = localStorage.getItem(key);
                break;
            case false:
                value = sessionStorage.getItem(key);
                break;
        }
        // handle null stored value. Will occur if nothing has been stored before.
        if (this.configuration.debugMode) {
            console.log('Retrieved from storage:');
            console.log(value);
        }
        value = value || '';

        // parse json, handle parsing error
        let parsedValue;
        try {
            parsedValue = JSON.parse(value);
        } catch (error) {
            console.log('Cached object stored in ' + (strongPersistence ? 'LocalStorage' : 'SessionStorage') + ' was empty.');
            parsedValue = null;
        }
        // return
        return parsedValue;
    }

  public getFromCookie(key: string): string {
    return this.parseCookie(key);
  }

  public saveToCookie(key: string, value: string): void {
    key = key || '';
    value = value || '';
    if (key === '') {
      return;
    }

    const cookieString = key + '=' + value + ';expires=Fri, 31 Dec 9999 23:59:59 GMT';
    document.cookie = cookieString;
  }

  private parseCookie(key: string): string {
    const result = document.cookie.match('(^|;)\\s*' + key + '\\s*=\\s*([^;]+)');
    return result ? result.pop() : '';
  }
}
