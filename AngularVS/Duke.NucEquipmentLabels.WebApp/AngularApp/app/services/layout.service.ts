import { Injectable, NgZone } from '@angular/core';

@Injectable()
export class LayoutService {
  constructor(private zone: NgZone) {}

  private get layoutHelpers(): any {
    return window['layoutHelpers'];
  }

  private exec(fn): any {
    return this.layoutHelpers && this.zone.runOutsideAngular(fn);
  }

  public getLayoutSidenav(): any {
    return this.exec(() => this.layoutHelpers.getLayoutSidenav()) || null;
  }

  public getSidenav(): any {
    return this.exec(() => this.layoutHelpers.getSidenav()) || null;
  }

  public getLayoutNavbar(): any {
    return this.exec(() => this.layoutHelpers.getLayoutNavbar()) || null;
  }

  public getLayoutFooter(): any {
    return this.exec(() => this.layoutHelpers.getLayoutFooter()) || null;
  }

  public getLayoutContainer(): any {
    return this.exec(() => this.layoutHelpers.getLayoutContainer()) || null;
  }

  public isSmallScreen(): any {
    return this.exec(() => this.layoutHelpers.isSmallScreen());
  }

  public isCollapsed(): any {
    return this.exec(() => this.layoutHelpers.isCollapsed());
  }

  public isFixed(): any {
    return this.exec(() => this.layoutHelpers.isFixed());
  }

  public isOffcanvas(): any {
    return this.exec(() => this.layoutHelpers.isOffcanvas());
  }

  public isNavbarFixed(): any {
    return this.exec(() => this.layoutHelpers.isNavbarFixed());
  }

  public isFooterFixed(): any {
    return this.exec(() => this.layoutHelpers.isFooterFixed());
  }

  public isReversed(): any {
    return this.exec(() => this.layoutHelpers.isReversed());
  }

  public setCollapsed(collapsed, animate = true): void {
    this.exec(() => this.layoutHelpers.setCollapsed(collapsed, animate));
  }

  public toggleCollapsed(animate = true): void {
    this.exec(() => this.layoutHelpers.toggleCollapsed(animate));
  }

  public setPosition(fixed, offcanvas): void{
    this.exec(() => this.layoutHelpers.setPosition(fixed, offcanvas));
  }

  public setNavbarFixed(fixed): void {
    this.exec(() => this.layoutHelpers.setNavbarFixed(fixed));
  }

  public setFooterFixed(fixed): void {
    this.exec(() => this.layoutHelpers.setFooterFixed(fixed));
  }

  public setReversed(reversed): void {
    this.exec(() => this.layoutHelpers.setReversed(reversed));
  }

  public update(): void {
    this.exec(() => this.layoutHelpers.update());
  }

  public setAutoUpdate(enable): void {
    this.exec(() => this.layoutHelpers.setAutoUpdate(enable));
  }

  public on(event, callback): void {
    this.exec(() => this.layoutHelpers.on(event, callback));
  }

  public off(event): void {
    this.exec(() => this.layoutHelpers.off(event));
  }

  public init(): void {
    this.exec(() => this.layoutHelpers.init());
  }

  public destroy(): void {
    this.exec(() => this.layoutHelpers.destroy());
  }

  // Internal
  //

  public _redrawLayoutSidenav(): void {
    this.exec(() => this.layoutHelpers._redrawLayoutSidenav());
  }

  public _removeClass(cls): void {
    this.exec(() => this.layoutHelpers._removeClass(cls));
  }
}
