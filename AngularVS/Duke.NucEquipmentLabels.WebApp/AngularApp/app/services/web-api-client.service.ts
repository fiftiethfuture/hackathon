import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../models/Constants';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/delay';
import { ConfigurationService } from './configuration.service';
import { ApiResponse } from '../models/ApiResponse';
import { Site } from '../models/Site';
import { LabelItem, LABEL_STATUSES } from '../models/LabelItem';
import { Equipment } from '../models/Equipment';
import { EquipmentSearchCriteria } from '../models/EquipmentSearchCriteria';


@Injectable({
    providedIn: 'root'
})
export class WebApiClientService {

    constructor(
        private httpClient: HttpClient,
        private configurationService: ConfigurationService
    ) {
        console.log(this.configurationService.config.nucEquipmentLabelsApiUrl);
    }

    public getSites(): Observable<ApiResponse<Site>> {
        const url = this.configurationService.config.nucEquipmentLabelsApiUrl + Constants.ENDPOINT_SITES;
        return this.httpClient.get<ApiResponse<Site>>(url);

        // dummy response code
        // const catawba: Site = new Site('CN', '(CN) Catawba');
        // const mcguire: Site = new Site('MC', '(MC) McGuire');
        // const oconee: Site = new Site('ON', '(ON) Oconee');
        // const fakeResponse = new ApiResponse<Site>();
        // fakeResponse.collection = [catawba, mcguire, oconee];
        // return Observable.of(fakeResponse).delay(1000);
    }

    public getEquipment(query: EquipmentSearchCriteria): Observable<ApiResponse<Equipment>> {
        query = query || new EquipmentSearchCriteria();
        const url = this.configurationService.config.nucEquipmentLabelsApiUrl + Constants.ENDPOINT_EQUIPMENT;
        const options = { params: query.toHttpParams() };
        return this.httpClient.get<ApiResponse<Equipment>>(url, options);

        // dummy response code
        // const fakeResponse = new ApiResponse<Equipment>();
        // const equipmentList: Equipment[] = [];
        // let counter = 0;
        // while (counter < 11) {
        //     equipmentList.push(
        //         new Equipment(
        //             '34343adf',
        //             '2',
        //             'CDF',
        //             'adf',
        //             'stuff',
        //             'thing',
        //             'fake',
        //             '2324242',
        //             'This equipment is cool.',
        //             'OCG#2',
        //             '6666',
        //             'My Fav power supply'
        //             ));
        //     counter++;
        // }
        // fakeResponse.collection = equipmentList;
        // return Observable.of(fakeResponse).delay(1000);
    }
}
