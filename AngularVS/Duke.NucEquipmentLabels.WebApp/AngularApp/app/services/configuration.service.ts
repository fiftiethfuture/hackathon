﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Configuration } from './../models/Configuration';
import { environment } from '../../environments/environment';
import { IConfiguration } from '../interfaces/IConfiguration';

//https://blogs.msdn.microsoft.com/webdev/2017/10/27/sharing-configuration-in-asp-net-core-spa-scenarios/
@Injectable() export class ConfigurationService {
    private readonly configUrlPath: string;
    private configData: Configuration;

    // A helper property to return the config object 
    public get config(): Configuration { return this.configData; }

    public get debugMode(): boolean {
        return !environment.production;
    }

    // Inject the http service and the app's BASE_URL constructor
    constructor(private http: Http) {
        this.configUrlPath = 'api/ClientConfiguration';
        this.configData = new Configuration();
    }

    private getBaseUrl(): string {
        return document.getElementsByTagName('base')[0].href;
    }

    // Call the ClientConfiguration endpoint, deserialize the response, 
    // and store it in this.configData.
    public loadConfigurationData(): Promise<Configuration> {
        if (environment.production) {
            console.log(`Running configuration with ${this.getBaseUrl()}`);
            return this.http.get(`${this.getBaseUrl()}${this.configUrlPath}`).toPromise()
                .then((response: Response) => {
                    const configuration: IConfiguration = response.json() || new Configuration();
                    this.configData.updatePropertiesWithIConfiguration(configuration);
                    this.configData.version = environment.version;
                    console.log(this.configData);
                    return this.configData;
                }).catch(err => {
                    return Promise.reject(err);
                });
        } else {
            console.log('Running configuration with environment variables locally.');
            this.configData = new Configuration();
            this.configData.nucEquipmentLabelsApiUrl = environment.nucEquipmentLabelsApiUrl;
            this.configData.helpUrl = environment.helpUrl;
            this.configData.environment = environment.name;
            this.configData.version = environment.version;
            console.log(this.configData);
            return Promise.resolve(this.configData);
        }
    }
}
