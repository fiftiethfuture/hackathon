﻿//adapted from https://github.com/scttcper/ngx-toastr/issues/137
//This service toasts and console logs the errors it's tasked with handling. 
import { Inject, Injector, NgZone } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { ConfigurationService } from '../services/configuration.service';

export class AppErrorHandler {
    constructor(@Inject(NgZone) private ngZone: NgZone,
        @Inject(Injector) private injector: Injector,
        private configurationService: ConfigurationService) { }

    protected get toastr(): ToastrService {
        return this.injector.get(ToastrService);
    }

    public handleError(error: Error): void {
        // toast
        this.ngZone.run(() => {
            let body = 'An error ocurred.';
            if (error instanceof HttpErrorResponse) {
                body += ` Status Code: ${error.status}`;
            }

            this.toastr.error(body, 'ERROR');
        });

        // log in console
        if (this.configurationService.debugMode) {
            console.error(`ERROR - ${error.message}`);
        }
    }
}