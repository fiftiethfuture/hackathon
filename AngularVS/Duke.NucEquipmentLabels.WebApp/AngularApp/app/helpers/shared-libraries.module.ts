// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// Libs
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  exports: [
    // Angular
    FormsModule,
    CommonModule,
    // Libs
    NgSelectModule
  ]
})
export class SharedLibrariesModule {}
