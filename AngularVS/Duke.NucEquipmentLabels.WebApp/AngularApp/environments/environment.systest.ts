export const environment = {
    production: false,
    name: 'SysTest',
    nucEquipmentLabelsApiUrl: 'https://nucwebapp4sys.duke-energy.com/NucEquipmentLabels_WS/api',
    helpUrl: '', // need to provide url for help video
    version: '1.0'
};
