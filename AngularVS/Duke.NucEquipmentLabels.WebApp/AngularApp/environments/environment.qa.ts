export const environment = {
    production: false,
    name: 'QA',
    nucEquipmentLabelsApiUrl: 'https://nucwebapp4qa.duke-energy.com/NucEquipmentLabels_WS/api',
    helpUrl: '', // need to provide url for help video
    version: '1.0'
};
