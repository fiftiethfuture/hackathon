export const environment = {
    production: true,
    name: 'PROD',
    nucEquipmentLabelsApiUrl: 'https://nucwebapp4.duke-energy.com/NucEquipmentLabels_WS/api',
    helpUrl: '', // need to provide url for help video
    version: '1.0'
};
