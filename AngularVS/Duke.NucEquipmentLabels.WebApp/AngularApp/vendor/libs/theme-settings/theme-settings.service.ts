import { Injectable, NgZone } from '@angular/core';

@Injectable()
export class ThemeSettingsService {
  constructor(private zone: NgZone) {}

  private get themeSettings(): any {
    return window['themeSettings'];
  }

  private exec(fn): any {
    return this.themeSettings && this.zone.runOutsideAngular(fn);
  }

  public get options(): ThemeSettingsOptions {
    return (this.themeSettings && this.themeSettings.settings) || {};
  }

  public getOption(name: string): any {
    return this.options[name] || null;
  }

  public setRtl(rtl: boolean): void {
    this.exec(() => this.themeSettings.setRtl(rtl));
  }

  public setMaterial(rtl: boolean): void {
    this.exec(() => this.themeSettings.setMaterial(rtl));
  }

  public setTheme(themeName: string, updateStorage = true, cb: () => any = null): void {
    this.exec(() => this.themeSettings.setTheme(themeName, updateStorage, cb));
  }

  public setLayoutPosition(pos: 'static' | 'static-offcanvas' | 'fixed' | 'fixed-offcanvas', updateStorage = true): void {
    this.exec(() => this.themeSettings.setLayoutPosition(pos, updateStorage));
  }

  public setLayoutNavbarFixed(fixed: boolean, updateStorage = true): void {
    this.exec(() => this.themeSettings.setLayoutNavbarFixed(fixed, updateStorage));
  }

  public setLayoutFooterFixed(fixed: boolean, updateStorage = true): void {
    this.exec(() => this.themeSettings.setLayoutFooterFixed(fixed, updateStorage));
  }

  public setLayoutReversed(reversed: boolean, updateStorage = true): void {
    this.exec(() => this.themeSettings.setLayoutReversed(reversed, updateStorage));
  }

  public setNavbarBg(bg: string, updateStorage = true): void {
    this.exec(() => this.themeSettings.setNavbarBg(bg, updateStorage));
  }

  public setSidenavBg(bg: string, updateStorage = true): void {
    this.exec(() => this.themeSettings.setSidenavBg(bg, updateStorage));
  }

  public setFooterBg(bg: string, updateStorage = true): void {
    this.exec(() => this.themeSettings.setFooterBg(bg, updateStorage));
  }

  public update(): void {
    this.exec(() => this.themeSettings.update());
  }

  public updateNavbarBg(): void {
    this.exec(() => this.themeSettings.updateNavbarBg());
  }

  public updateSidenavBg(): void {
    this.exec(() => this.themeSettings.updateSidenavBg());
  }

  public updateFooterBg(): void {
    this.exec(() => this.themeSettings.updateFooterBg());
  }

  public clearLocalStorage(): void {
    this.exec(() => this.themeSettings.clearLocalStorage());
  }

  public destroy(): void {
    this.exec(() => this.themeSettings.destroy());
  }
}

export interface ThemeSettingsTheme {
  name: string;
  title: string;
  colors: {
    primary: string;
    navbar: string;
    sidenav: string;
  };
}

export interface ThemeSettingsOptions {
  cssPath: string;
  themesPath: string;
  cssFilenamePattern: string;
  navbarBgs: string[];
  defaultNavbarBg: string;
  sidenavBgs: string[];
  defaultSidenavBg: string;
  footerBgs: string[];
  defaultFooterBg: string;
  availableThemes: ThemeSettingsTheme[];
  defaultTheme: ThemeSettingsTheme;
  theme: ThemeSettingsTheme;
  controls: string[];
  rtl: boolean;
  material: boolean;
  layoutPosition: 'static' | 'static-offcanvas' | 'fixed' | 'fixed-offcanvas';
  layoutReversed: boolean;
  layoutNavbarFixed: boolean;
  layoutFooterFixed: boolean;
  navbarBg: string;
  sidenavBg: string;
  footerBg: string;
  pathResolver: (url: string) => string;
}
