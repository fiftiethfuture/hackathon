﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NucMobile.Hackathon.WebApp.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NucMobile.Hackathon.WebApp.Controllers
{
    [Route("api/[controller]")]
    public class ClientConfigurationController : Controller
    {
        private readonly ClientConfig _clientConfig;

        public ClientConfigurationController(IOptionsSnapshot<ClientConfig> clientConfigOptions)
        {
            _clientConfig = clientConfigOptions?.Value;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return Json(_clientConfig);
        }
    }
}
