﻿using Microsoft.AspNetCore.Mvc;

namespace NucMobile.Hackathon.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
