﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NucMobile.Hackathon.WebApp.Models
{
    public class ClientConfig
    {
        public string NucEquipmentLabelsApiUrl { get; set; }
        public string HelpUrl { get; set; }
        public string Environment { get; set; }
    }
}
